$(()=> {
    var i = 0;
    var tableau = [];
    $('#envoyer-1').click(() => {
        var taille = document.getElementById("taille").value;
        //console.log(taille);
    
        $('#envoyer-2').click(()=> {
            if(i < taille) {
                var chiffre = document.getElementById("tab").value;
                tableau.push(chiffre);
                console.log(chiffre);
                i++;
            } else { //i > taille, accès au tableau entier
                console.log(tableau);
                document.getElementById("envoyer-2").value = "Generate";
                document.getElementById("envoyer-2").addEventListener
                ("mouseover", function() {
                    document.getElementById("moyenne").textContent = moyenne(tableau);
                    document.getElementById("variance").textContent = variance(tableau);
                    document.getElementById("ecart-type").textContent = ecartType(tableau);

                });
            }
        });
    });

    //RESET -- recharge la page
    document.getElementById("reset-1").addEventListener("click", function() {
        document.location.reload(true);
    });
    document.getElementById("reset-2").addEventListener("click", function() {
        document.location.reload(true);
    });

    //Fonction Secondaire
    //Fonction de la moyenne
    function moyenne(tableau) {
        var sum = 0 ;
        var calcul=0;
        for (var i=0; i<tableau.length; i++) {
            sum+=parseInt(tableau[i]);
        }
        calcul = sum / tableau.length;
        return calcul;
    }
    //Fonction de la variance
    function variance(tableau) {
        var moy = moyenne(tableau);
        var sum = 0;
        var varia = 0;
        for (var i=0; i<tableau.length;i++) {
            sum+= parseInt(tableau[i] - moy) * (parseInt(tableau[i] - moy));
        }
        varia = sum / tableau.length;
        return varia;
    }
    //Fonction de la ecart Type
    function ecartType(tableau) {
        var a = variance(tableau);
        return Math.sqrt(a);
    }
});