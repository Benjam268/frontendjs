$(()=> {

    //Affichage une seule fois le triangle de pascal
    var boolPascal = true;
    var boolStern = true;



    $("#valider").click(() => {
        if (boolPascal==true) {
            var taille = document.getElementById("infos").value;

            afficherTriangle(TrianglePascal(taille));
            boolPascal = false;
        }
        
    });


    //Stern
    $("#valider-1").click(() => {
        if (boolStern==true) {
            var numero = document.getElementById("stern").value;

            afficherStern(Stern(numero));
            boolStern = false;
        }
        
    });

    //RESET -- recharge la page
    document.getElementById("retry").addEventListener("click", function() {
        document.location.reload(true);
    });
    document.getElementById("retry-1").addEventListener("click", function() {
        document.location.reload(true);
    });






    //fonction secondaire
    // Generation du triangle
    function TrianglePascal(taille) {
        var tab = new Array(); // tab = []
        for (var n=0; n<taille; n++) { // parcours nb de ligne
            tab[n] = new Array();
            for(var p=0; p<n+1; p++) {
                if(p==0 || p==n) {
                    tab[n][p] =1;
                } else {
                    tab[n][p]=tab[n-1][p] + tab[n-1][p-1];
                }
                
            }
        }
        return tab;
    }
    
    //Fonction d'affichage ne retourne rien
    function afficherTriangle(tab) {
        for (var i=0; i<tab.length; i++) {
            for(var j=0; j<tab[i].length; j++) {
                document.getElementById("res").innerHTML += tab[i][j] + " ";
            }
            document.getElementById("res").innerHTML += "<br/>";
        }
    }

    
    //Partie Stern
    function Stern(numero) {
        var tab1 = TrianglePascal(numero+100)
        var tab2 = [];
        tab2.push(1);
        tab2.push(1);
        tab2.push(2);
        tab2.push(1);
        //tab2 = [1,1,2,1]
        for(var i=4; i<numero; i++) {
            var sum = 0;
            var newval = Math.trunc(i/2); // Récupération de la valeur entière 
            for(var j=0; j<newval+1 ; j++) {
                sum += tab1[i-j][j]%2;
            }
            tab2.push(sum);
        }
        return tab2;
    }
    //console.log(Stern(7));
    

    //Affichage de stern
    function afficherStern(tab) {
        for(var i=0; i<tab.length; i++) {
            var u = i+1;
            document.getElementById("stern-res").innerHTML += "S"+u+"="+
            " "+tab[i]+" ";
        }
    }
});